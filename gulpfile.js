var gulp = require('gulp');
var less = require('gulp-less');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');
var useref = require('gulp-useref');
var cssnano = require('gulp-cssnano');
var gulpIf = require('gulp-if');
var uglify = require('gulp-uglify');
var minifyCSS = require('gulp-minify-css');
var del = require('del');
var cache = require('gulp-cache');
var imagemin = require('gulp-imagemin');
var runSequence = require('run-sequence');

gulp.task('browserSync', function() {
  browserSync({
    server: {
      baseDir: './app'
    }
  })
});

gulp.task('sass', function () {
  gulp.src('./app/sass/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./app/css'));
});

gulp.task('useref', function() {

  return gulp.src('./app/*.html')
    .pipe(useref())
    .pipe(gulpIf('*.js', uglify()))
    .pipe(gulpIf('*.css', cssnano()))
    .pipe(gulp.dest('dist'));
});

gulp.task('images', function() {
  return gulp.src('./app/images/**/*.+(png|jpg|jpeg|gif|svg)')
    .pipe(imagemin([
        imagemin.gifsicle({
          interlaced: true,
          progressive: true

        }),
        imagemin.jpegtran({
          interlaced: true,
          progressive: true

        }),
        imagemin.jpegtran({
          interlaced: true,
          progressive: true
        }),
        imagemin.svgo()
      ]
    ))
    .pipe(cache(imagemin({
      interlaced: true,
      progressive: true,
      optimizationLevel: 5
    })))
    .pipe(gulp.dest('./dist/images'))
});

gulp.task('fonts', function() {
  return gulp.src('./app/fonts/**/*')
    .pipe(gulp.dest('dist/fonts'))
});

gulp.task('favicon', function() {
  return gulp.src('./app/favicon/**/*')
    .pipe(gulp.dest('dist/favicon'))
});

gulp.task('watch', function (){
  gulp.watch('./app/sass/**/*.scss', ['sass']);
  gulp.watch('./app/**/*.html', browserSync.reload);
  gulp.watch('./app/js/**/*.js', browserSync.reload);
});

gulp.task('clean', function() {
  return del.sync('dist').then(function(cb) {
    return cache.clearAll(cb);
  });
});

gulp.task('clean:dist', function() {
  return del.sync(['dist/**/*', '!dist/images', '!dist/images/**/*']);
});

gulp.task('build', function(callback) {
  runSequence(
    'clean:dist',
    'sass',
    'fonts',
    'images',
    'favicon',
    'useref',
    callback
  )
});
